@isTest
public with sharing class ContactManagerController_Test {
    
    @TestSetup
    static void makeData()
    {
        Account acc1 = new Account(Name='Acc1');
        Account acc2 = new Account(Name='Acc2');
        insert acc1;
        insert acc2;

        Contact con1 = new Contact(LastName='Smith', AccountId=acc1.Id);
        Contact con2 = new Contact(LastName='Jones', AccountId=acc1.Id);
        Contact con3 = new Contact(LastName='Hammer', AccountId=acc2.Id);
        insert con1;
        insert con2;
        insert con3;
    }

    @isTest
    static void doesGetVisibleAccountFieldsReturnProper()
    {
        Test.startTest();
        List<FieldOption> fields = ContactManagerController.getVisibleAccountFields();
        Test.stopTest();

        Boolean nameFieldFound = false;
        for(FieldOption eachField : fields) {
            if(eachField.value == 'Name') {
                System.assertEquals('Account Name', eachField.label);
                System.assertEquals('text', eachField.fieldType);
                System.assertEquals(Schema.DisplayType.STRING.ordinal(), eachField.typeOrdinal);
            }
        }
    }

    @isTest
    static void doesDeleteContactsDeleteContacts()
    {
        Contact deleteCon = [SELECT Id FROM Contact WHERE LastName = 'Smith' LIMIT 1];
        List<Id> deleteIds = new List<Id>{deleteCon.Id};

        Test.startTest();
        ContactManagerController.deleteContacts(deleteIds);
        Test.stopTest();

        List<Contact> conResult = [SELECT Id FROM Contact WHERE LastName = 'Smith'];
        System.assertEquals(0, conResult.size());
    }

    @isTest
    static void doesSearchForAccountsReturnProper()
    {
        List<ContactManagerController.CriteriaRowData> criteriaRows = new List<ContactManagerController.CriteriaRowData>();
        ContactManagerController.CriteriaRowData payload = new ContactManagerController.CriteriaRowData();
        payload.fieldName = 'Name';
        payload.searchType = 'equal';
        payload.value = 'Acc1';
        criteriaRows.add(payload);

        Test.startTest();
        List<Account> foundAccs = ContactManagerController.searchForAccounts(criteriaRows);
        Test.stopTest();

        System.assertEquals(1, foundAccs.size());
    }
}
