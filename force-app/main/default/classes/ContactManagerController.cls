public with sharing class ContactManagerController {

    private static final List<Integer> NUMERIC_FIELD_TYPES = new List<Integer>{
        Schema.DisplayType.CURRENCY.ordinal(),
        Schema.DisplayType.DATE.ordinal(),
        Schema.DisplayType.DATETIME.ordinal(),
        Schema.DisplayType.INTEGER.ordinal(),
        Schema.DisplayType.DOUBLE.ordinal(),
        Schema.DisplayType.PERCENT.ordinal(),
        Schema.DisplayType.TIME.ordinal()
    };
    
    @AuraEnabled(cacheable=true)
    public static List<FieldOption> getVisibleAccountFields()
    {
        List<FieldOption> result = new List<FieldOption>();

        for (SObjectField eachField : Account.sObjectType.getDescribe().fields.getMap().values()) {
            Schema.DescribeFieldResult F = eachField.getDescribe();
            if(F.isAccessible() && F.isFilterable()) {
                result.add(new FieldOption(F.getLabel(), F.getName(), F.getType()));
            }
        }
        result.sort();
        return result;
    }

    @AuraEnabled(cacheable=true)
    public static List<Account> searchForAccounts(List<CriteriaRowData> criteriaRows){
        List<Account> foundAccs = new List<Account>();

        String queryFields = ' Id, Name, Phone, Website, Industry, Type,' + 
                            '(SELECT Id, LastName, FirstName, Phone, Email FROM Contacts ORDER BY LastName ASC) ';
        List<String> whereClauses = new List<String>();
        
        for(CriteriaRowData eachRow : criteriaRows) {
            String clause = '';
            if(eachRow.searchType == 'contain') {
                clause = eachRow.fieldName + ' LIKE \'%' + eachRow.value + '%\'';
            }
            else if(eachRow.searchType != 'equal') {
                clause = eachRow.fieldName + ' ' + getSearchOperator(eachRow.searchType) + ' ' + 
                        getLiteralValue(eachRow.typeOrdinal, eachRow.value);
            }
            else if(eachRow.searchType == 'between') {
                clause = eachRow.fieldName + ' >= ' + eachRow.minValue + ' AND ' +
                                eachRow.fieldName + ' <= ' + eachRow.maxValue;
            }
            whereClauses.add(clause);
        }

        String query = 'SELECT ' + queryFields + ' FROM Account WHERE ' + String.join(whereClauses, ' AND ') + 'ORDER BY Name ASC';
        System.debug(query);
        foundAccs = Database.query(query);
        return foundAccs;
    }

    @AuraEnabled
    public static void deleteContacts(List<Id> conIds){
        System.debug('START deleteContacts');
        System.debug(conIds);
        Database.delete(conIds);
    }

    private static String getSearchOperator(String searchType)
    {
        switch on searchType {
            when 'equal' {
                return '=';
            }
            when 'contain' {
                return 'IN';
            }
            when 'greater' {
                return '>=';
            }
            when 'less' {
                return '<=';
            }
            when else {
                return null;
            }
        }
    }

    private static String getLiteralValue(Integer typeOrdinal, Object value)
    {
        System.debug('getLiteralValue :: ' + typeOrdinal);
        if(NUMERIC_FIELD_TYPES.contains(typeOrdinal)) {
            return '' + value;
        } else {
            return '\'' + value + '\'';
        }
    }

    @TestVisible
    private class CriteriaRowData{
        @AuraEnabled
        public String fieldName {get; set;}
        @AuraEnabled
        public String searchType {get; set;}
        @AuraEnabled
        public Integer typeOrdinal {get; set;}
        @AuraEnabled
        public Object minValue {get; set;}
        @AuraEnabled
        public Object maxValue {get; set;}
        @AuraEnabled
        public Object value {get; set;}
    }
}
