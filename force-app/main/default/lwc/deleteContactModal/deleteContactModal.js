import { api, LightningElement } from 'lwc';

export default class DeleteContactModal extends LightningElement {

    @api accountId;
    @api cons;

    selected;

    get conOptions() {
        let options =  this.cons.map(eachCon => {
            return {
                label: eachCon.FirstName + ' ' + eachCon.LastName,
                value: eachCon.Id
            }
        });
        console.log('options:');
        console.log(options);
        return options;
    }

    // Handlers
    handleClose() {
        console.log('deleteContactModal :: handleClose');
        this.dispatchEvent(new CustomEvent('close'));
    }

    handleSubmit() {
        console.log('deleteContactModal :: handleSubmit');
        this.dispatchEvent(new CustomEvent('submit', {detail: this.selected}));
    }

    handleSelectedConChange(event) {
        console.log('deleteContactModal :: handleSelectedConChange');
        // console.log(JSON.parse(JSON.stringify(event.detail.value)));
        this.selected = event.detail.value;
    }
}