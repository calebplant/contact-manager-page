import { LightningElement, wire } from 'lwc';
import getVisibleFields from '@salesforce/apex/ContactManagerController.getVisibleAccountFields';

export default class ContactManagerSearch extends LightningElement {

    criteriaOptions;
    typeDict;
    ordinalDict;
    numOfCriteria = [''];
    numOfCriteriaValue = 1;
    
    @wire(getVisibleFields)
    getVisibleFields(response) {
        if(response.data) {
            this.criteriaOptions = [];
            this.typeDict = [];
            this.ordinalDict = {};
            console.log('Successfully retreived fields.');
            console.log(response.data);

            try {
                this.criteriaOptions = response.data.map(eachField => {
                    return {
                        label: eachField.label,
                        value: eachField.value
                    };
                });
                response.data.forEach(eachField => {
                    this.typeDict[eachField.value] = eachField.fieldType
                    this.ordinalDict[eachField.value] = eachField.typeOrdinal;
                });
            } catch(err) {
                console.log('error:');
                console.log(err);
            }


            console.log(this.criteriaOptions);
            console.log(this.typeDict);
        }
        if(response.error) {
            console.log('Error retreiving fields!');
            console.log(response.error);
        }
    }

    get numOfCriteriaOptions() {
        return [
            {label: 1, value: 1},
            {label: 2, value: 2},
            {label: 3, value: 3},
            {label: 4, value: 4},
            {label: 5, value: 5}
        ];
    }

    // Handlers
    handleSearch() {
        console.log('contactManagerSearch :: handleSearch');
        let criteriaRows = this.template.querySelectorAll('c-account-criteria-row');
        let payload = [...criteriaRows].map(eachRow => eachRow.getRowData());
        // console.log('payload');
        // console.log(JSON.parse(JSON.stringify(payload)));
        this.dispatchEvent(new CustomEvent('search', {detail: payload}));

    }

    handleReset() {
        console.log('contactManagerSearch :: handleReset');
        this.dispatchEvent(new CustomEvent('reset'));
        let criteriaRows = this.template.querySelectorAll('c-account-criteria-row');
        this.numOfCriteria = [''];
        this.numOfCriteriaValue = 1;
        criteriaRows.forEach(eachRow => eachRow.resetData());
    }

    handleNumOfCriteriaSelected(event) {
        console.log('contactManagerSearch :: handleNumOfCriteriaSelected');
        this.numOfCriteria = [];
        for(let i=0; i < event.detail.value; i++) {
            this.numOfCriteria.push('');
        }
        this.numOfCriteriaValue = this.numOfCriteria.length;
    }

}