import { api, LightningElement } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';


export default class AddContactForm extends LightningElement {
    @api parentAccountId;
    @api layoutFields;

    get leftColumn() {
        console.log('addContactForm :: leftColumn');
        let fields = this.layoutFields.left.map(eachField => {
            if(eachField != 'AccountId') {
                return {name: eachField, value: ''};
            } else {
                return {name: eachField, value: this.parentAccountId};
            }
        });
        return fields;
    }

    get rightColumn() {
        console.log('addContactForm :: rightColumn');
        let fields = this.layoutFields.right.map(eachField => {
            if(eachField != 'AccountId') {
                return {name: eachField, value: null};
            } else {
                return {name: eachField, value: this.parentAccountId};
            }
        });
        return fields;
    }

    // Handlers
    handleError(event) {
        console.log('Error creating Contact.');
        console.log(JSON.parse(JSON.stringify(event.detail)));
        // this.dispatchEvent(new ShowToastEvent({
        //     'title': 'Error creating contact!',
        //     'message': 'See top of form for details.',
        //     'variant': 'error'
        // }));
    }

    handleSuccess(event) {
        console.log('Created contact: ' + event.detail.id);
        this.dispatchEvent(new ShowToastEvent({
            'title': 'Success!',
            'message': 'Successfully created contact: ' + event.detail.id,
            'variant': 'success'
        }));
        this.dispatchEvent(new CustomEvent('created'));
    }

    handleReset() {
        console.log('addContactForm :: handleReset');
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field');
        if (inputFields) {
            inputFields.forEach(field => {
                field.reset();
            });
        }
    }
}