import { api, LightningElement } from 'lwc';

const COLUMNS = [
    {label: 'Last Name', fieldName: 'LastName', type: 'text'},
    {label: 'First Name', fieldName: 'FirstName', type: 'text'},
    {label: 'Phone', fieldName: 'Phone', type: 'phone'},
    {label: 'Email', fieldName: 'Email', type: 'email'},
];

export default class ContactInfoDisplay extends LightningElement {

    @api parentAccountId;
    @api layoutFields;
    @api conData;
    @api showCreate;
    columns = COLUMNS;
    showCreateForm = false;

    get showNoConsFoundMessage() {
        return this.conData ? false : true;
    }

    // Handlers
    handleCreated() {
        console.log('ContactInfoDisplay :: handleCreated');
        this.showCreate = false;
        this.dispatchEvent(new CustomEvent('created'));
    }
}