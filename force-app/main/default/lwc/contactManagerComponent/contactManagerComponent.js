import { LightningElement, wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { getRecordUi } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { refreshApex } from '@salesforce/apex';


import searchForAccounts from '@salesforce/apex/ContactManagerController.searchForAccounts';
import deleteContacts from '@salesforce/apex/ContactManagerController.deleteContacts';

const MASTER_RECORD_TYPE_ID = '012000000000000AAA';
const REFERENCE_CONTACT_ID = '0033D00000i4odPQAQ';

export default class ContactManagerComponent extends NavigationMixin(LightningElement) {
    
    accountData;
    selectedAccountId;
    relatedCons;
    layoutFields;
    searchCriteria;
    showCreateContact = false;
    showConInfo = false;
    showDeleteModal = false;

    _response;

    @wire(getRecordUi, { recordIds: REFERENCE_CONTACT_ID, layoutTypes: 'Full', modes: 'View' })
    contactRecordUi(response){
        if(response.data) {
            console.log('Successfully got Contact ui.');
            console.log(response.data);
            this.layoutFields = this.parseLayoutFields(response.data);
        }
        if(response.error) {
            console.log('Error getting Contact ui!');
            console.log(response.error);
        }
    }

    @wire(searchForAccounts, {criteriaRows: '$searchCriteria'})
    runAccountSearch(response) {
        if(response.data) {
            console.log('Successfully got accounts.');
            console.log(response.data);
            this.accountData = response.data;
            if(this.selectedAccountId) {
                this.accountData.forEach(eachAcc => {
                    if(eachAcc.Id == this.selectedAccountId) {
                        this.relatedCons = eachAcc.Contacts;
                    }
                });
            }
            this._response = response;
        }
        if(response.error) {
            console.log('Error getting accounts!');
            console.log(response.error);
            this.dispatchEvent(new ShowToastEvent({
                'title': 'Error searching accounts!',
                'message': response.error.body.message,
                'variant': 'error'
            }));
        }
    }

    // Handlers
    handleAccountSearch(event) {
        console.log('contactManagerComponent :: handleAccountSearch');
        this.handleSearchReset();
        this.searchCriteria = event.detail;
    }

    handleCreateCon(event) {
        console.log('contactManagerComponent :: handleCreateCon');
        this.selectedAccountId = event.detail;
        this.showConInfo = true;
        this.showCreateContact = true;
    }

    handleDeleteCon(event) {
        console.log('contactManagerComponent :: handleDeleteCon');

        this.selectedAccountId = event.detail;
        this.accountData.forEach(eachAcc => {
            if(eachAcc.Id == this.selectedAccountId) {
                this.relatedCons = eachAcc.Contacts;
            }
        });
        this.showDeleteModal = true;
    }

    handleDeleteModalClose(event) {
        console.log('contactManagerComponent :: handleDeleteModalClose');
        this.showDeleteModal = false;
    }

    handleDeleteModalSubmit(event) {
        console.log('contactManagerComponent :: handleDeleteModalSubmit');
        this.showDeleteModal = false;

        deleteContacts({
            conIds: event.detail
        })
        .then(result => {
            console.log('Successfully deleted contacts.');
            this.dispatchEvent(new ShowToastEvent({
                'title': 'Success!',
                'message': 'Successfully created contact: ' + event.detail.id,
                'variant': 'success'
            }));
            refreshApex(this._response);
        })
        .catch(error => {
            console.log('Error deleting contacts!');
            console.log(error);
        });
    }

    handleSelectAccount(event) {
        console.log('contactManagerComponent :: handleSelectAccount');
        this.selectedAccountId = event.detail;
        this.accountData.forEach(eachAcc => {
            if(eachAcc.Id == this.selectedAccountId) {
                this.relatedCons = eachAcc.Contacts;
            }
        });
        this.showConInfo = true;
    }

    handleConCreated() {
        console.log('contactManagerComponent :: handleConCreated');
        this.showCreateContact = false;
        refreshApex(this._response);
    }

    handleSearchReset() {
        this.accountData = null;
        this.selectedAccountId = null;
        this.relatedCons = null;
        this.showConInfo = false;
    }

    // Utils
    parseLayoutFields(data) {
        console.log('contactManagerComponent :: parseLayoutFields');
        let left = [];
        let right = [];
        let excludedFields = ['CreatedById', 'CreatedDate', 'LastModifiedById', 'LastModifiedDate', 'OwnerId'];
        let sections = data.layouts.Contact[MASTER_RECORD_TYPE_ID].Full.View.sections;
        sections.forEach(eachSection => {
            let col = 1;
            eachSection.layoutRows.forEach(eachRow => {
                eachRow.layoutItems.forEach(eachItem => {
                    col++;
                    eachItem.layoutComponents.forEach(eachComponent => {
                        if(!excludedFields.includes(eachComponent.apiName)) {
                            if(col % 2 == 0) {
                                left.push(eachComponent.apiName);
                            } else {
                                right.push(eachComponent.apiName);
                            }
                        }
                    })
                })
            });
        });
        console.log('fields....');
        console.log(left);
        console.log(right);
        return {left: left, right: right};
    }
}