import { api, LightningElement } from 'lwc';

const BOOLEAN_FIELDS = ['checkbox'];
const NUMERIC_FIELDS = ['date', 'datetime', 'time', 'number'];
const TEXT_FIELDS = ['email', 'tel', 'text', 'url'];
const RANGE_FIELDS = ['between'];

export default class AccountCriteriaRow extends LightningElement {
    
    // @api hideRemove;
    @api criteriaOptions;
    @api typeDict;
    @api ordinalDict;

    @api
    getRowData(){
        console.log('accountCriteriaRow :: getRowData');
        let data = {};
        data['fieldName'] = this.selectedCriteria;
        data['searchType'] = this.selectedSearchType;
        data['typeOrdinal'] = this.ordinalDict[this.selectedCriteria];
        data['minValue'] = this.minValue;
        data['maxValue'] = this.maxValue;
        data['value'] = this.value;
        console.log(data);
        return data;
    }

    @api
    resetData(){
        this.selectedCriteria = null;
        this.criteriaType = 'text';
        this.selectedSearchType = null;
        this.minValue = null;
        this.maxValue = null;
        this.value = null;
    }
    
    selectedCriteria;
    criteriaType = 'text';
    selectedSearchType;
    
    minValue;
    maxValue;
    value;

    get isBoolean() {
        return BOOLEAN_FIELDS.includes(this.criteriaType);
    }

    get isNumeric() {
        return NUMERIC_FIELDS.includes(this.criteriaType);
    }

    get isText() {
        return TEXT_FIELDS.includes(this.criteriaType);
    }

    get booleanOptions() {
        return [
            {label: 'Equals', value:'equal'}
        ];
    }

    get numericOptions() {
        return [
            {label: 'Equals', value:'equal'},
            {label: 'Greater Than', value:'greater'},
            {label: 'Less Than', value:'less'},
            {label: 'Between', value:'between'},
        ];
    }

    get textOptions() {
        return [
            {label: 'Equals', value:'equal'},
            {label: 'Contains', value:'contain'},
        ]
    }

    get isRange() {
        return RANGE_FIELDS.includes(this.selectedSearchType);
    }

    // Handlers
    handleCriteriaChange(event) {
        console.log('accountCriteriaRow :: handleCriteriaChange');
        this.selectedCriteria = event.detail.value;
        this.criteriaType = this.typeDict[event.detail.value];
        console.log(this.selectedCriteria);
    }

    handleSearchTypeChange(event) {
        console.log('accountCriteriaRow :: handleSearchTypeChange');
        this.selectedSearchType = event.detail.value;
        console.log(this.selectedSearchType);
    }

    handleMinValueChange(event) {
        this.minValue = event.detail.value;
    }

    handleMaxValueChange(event) {
        this.maxValue = event.detail.value;
    }

    handleValueChange(event) {
        this.value = event.detail.value;
    }

    handleRemove(event) {

    }

}