import { api, LightningElement } from 'lwc';

const COLUMNS = [
    {label: 'Name', fieldName: 'Name', type: 'text'},
    {label: 'Phone', fieldName: 'Phone', type: 'phone'},
    {label: 'Website', fieldName: 'Website', type: 'url'},
    {label: 'Industry', fieldName: 'Industry', type: 'text'},
    {label: 'Type', fieldName: 'Type', type: 'text'},
    {type: "button-icon", fixedWidth: 50, typeAttributes: {  
        name: 'addContact',  
        title: 'Add Contact',
        iconName: 'action:add_contact',
        variant: 'brand'
    }}, 
    {type: "button-icon", fixedWidth: 50, typeAttributes: {  
        name: 'deleteContact',  
        title: 'Delete Contact',
        iconName: 'action:delete',
        variant: 'brand'
    }}
];


export default class AccountInfoDisplay extends LightningElement {

    @api accountData;
    columns = COLUMNS;
    previousSelectedRow;
    selectedRow = [];

    get showNoResultsMessage() {
        return this.accountData.length > 0 ? false : true;
    }

    // Handlers
    handleRowSelect(event) {
        console.log('accountInfoDisplay :: handleRowSelect');

        // Updated selected row
        if(this.previousSelectedRow) {
            for(let i=0; i < event.detail.selectedRows.length; i++) {
                if(event.detail.selectedRows[i].Id != this.previousSelectedRow) {
                    this.previousSelectedRow = event.detail.selectedRows[i].Id;
                    this.selectedRow = [event.detail.selectedRows[i].Id];
                    break;
                }
            }
        } else {
            this.selectedRow = event.detail.selectedRows.map(r => r.Id);
            this.previousSelectedRow = this.selectedRow[0];
        }
        // Dispatch event
        if(this.selectedRow.length > 0) {
            this.dispatchEvent(new CustomEvent('rowselected', {detail: this.previousSelectedRow}));
        }
    }

    handleRowAction(event) {
        console.log('accountInfoDisplay :: handleRowAction');
        console.log(JSON.parse(JSON.stringify(event.detail.row)));

        switch(event.detail.action.name) {
            case 'addContact':
                this.openCreateContactModal(event.detail.row.Id);
                break;
            case 'deleteContact':
                this.openDeleteContactModal(event.detail.row.Id);
                break;
        }

        // event.detail.action.name => Returns the value of the name set in typeAttributes.
        // event.detail.row => Returns an Object with the values of the row
        // event.detail.row.columnFieldName => Returns the value in an specific column
    }

    // Utils
    openCreateContactModal(accountId) {
        console.log('accountInfoDisplay :: openCreateContactModal');
        this.dispatchEvent(new CustomEvent('createcon', {detail: accountId}));
    }

    openDeleteContactModal(accountId) {
        console.log('accountInfoDisplay :: openDeleteContactModal');
        this.dispatchEvent(new CustomEvent('deletecon', {detail: accountId}));
    }
}