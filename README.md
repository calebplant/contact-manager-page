# Contact Manager Page

## Overview

A custom page that lets the user search for Accounts based on criteria and then manage the contacts for the resultant accounts.

## Demo (~1:40 min)

[Here's a link to a demo video for the component](https://www.youtube.com/watch?v=WMYzt8pUYI4)

## Some Screenshots

### Searching Accounts and Viewing Related Contacts

![Searching Accounts and Viewing Related Contacts](media/account_search.png)

### Deleting Contacts From an Account

![Deleting Contacts From an Account](media/delete_con.png)

### Creating Contacts For an Account

![Deleting Contacts From an Account](media/add_con.png)

## File Overview

### LWC

* **contactManagerComponent** - Main component that contains all three sections of the overall page and controls modal display.
* **contactManagerSearch** - Top section that contains accountCriteriaRows for the user to enter search criteria into.
* **accountCriteriaRow** - Helper for contactManagerSearch that lets a user enter a search criteria (what field, what operation, values).
* **accountInfoDisplay** - Middle section showing info about accounts that were found from the search.
* **contactInfoDisplay** - Bottom section of the page that contains the Add Contact form and a datatable with info about existing contacts associated with the selected account.
* **addContactForm** - Dynamic form for creating new contacts associated with the selected account.
* **deleteContactModal** - Modal for deleting contacts associated with the selected account.

### Apex

* **ContactManagerController** - Controller for the component. Retrieves user-visible search fields, runs the account search, and deletes contacts.